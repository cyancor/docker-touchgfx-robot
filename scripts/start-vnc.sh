#!/bin/bash

echo "Starting VNC server..."
export DISPLAY=:1

if [ "$RESOLUTION" != "" ]; then
    vncserver -geometry $RESOLUTION $DISPLAY &
else
    vncserver $DISPLAY &
fi
