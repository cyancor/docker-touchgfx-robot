#!/bin/bash

XAvailable=$(/xprobe; echo $?)
if [ "$XAvailable" != "0" ]; then echo "Setting DISPLAY to :0 (was \"$DISPLAY\")"; export DISPLAY=":0"; fi
XAvailable=$(/xprobe; echo $?)
if [ "$XAvailable" != "0" ]; then echo "Setting DISPLAY to host.docker.internal:0 (was \"$DISPLAY\")"; export DISPLAY="host.docker.internal:0"; fi
XAvailable=$(/xprobe; echo $?)
if [ "$XAvailable" == "0" ]; then
    printf "\n\033[1;32mXServer Connection available.\033[0m\n\n"
else
    printf "\n\033[1;31mXServer Connection not available.\033[0m\n\n"
fi

exit $XAvailable