FROM ubuntu:focal
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

EXPOSE 5901
EXPOSE 6080

ENV SHELL="/bin/bash" \
    DEBIAN_FRONTEND="noninteractive" \
    PATH="$PATH:/gcc-arm-none-eabi/bin:/opt/jython/bin/:/usr/local/lib/python3.8/dist-packages:/usr/games" \
    USER="root" \
    TERM="xterm" \
    JYTHON_VERSION="2.7.2"

COPY scripts/* /
COPY configurations/xfce4-panel.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
COPY resources/libpng12-0.deb /tmp/libpng12-0.deb
COPY scripts/start-vscode.sh /start-vscode.sh

RUN apt-get update \
    && apt-get install --yes \
        # Base packages
        ssh \
        git \
        curl \
        wget \
        htop \
        apt-utils \
        locales \
        openjdk-8-jdk \
        avahi-daemon \
        avahi-discover \
        avahi-utils \
        libnss-mdns \
        mdns-scan \
        telnet \
        iputils-ping \
        iproute2 \
        lsof \
        cowsay \
        imagemagick \
        libxml2-utils \
        netcat \
        vlc \
        software-properties-common \
        zip \
        unzip \
        gnupg2 \
        mono-runtime \
        nano \
        # Build packages
        cmake \
        clang \
        clang-tidy \
        clang-format \
        ccache \
        ruby-full \
        ruby-dev \
        zlib1g-dev \
        libtclap-dev \
        # Python
        python3-distutils \
        python3-apt \
        # UI
        xfce4 \
        xfce4-goodies \
        # VNC
        tightvncserver \
        # Development
        gdb \
# Initial configuration
    && printf "\n\033[0;36mInitial configuration...\033[0m\n" \
    && locale-gen en_US.UTF-8 \
    && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 \
    && export LANG=en_US.UTF-8 \
# Python 3, pip
    && printf "\n\033[0;36mInstalling Python...\033[0m\n" \
    && printf '#!/bin/bash\npython3 "$@"\n' > /usr/bin/python \
    && chmod 0777 /usr/bin/python \
    && wget https://bootstrap.pypa.io/get-pip.py \
    && python3 get-pip.py \
    && rm get-pip.py \
# cppclean
    && pip install --upgrade cppclean \
# ccache
    && mkdir -p /.ccache \
    && chmod 0777 /.ccache \
# Compilers
    && curl -o /gcc-arm-none-eabi.tar.bz2 "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2" \
    && tar xjf /gcc-arm-none-eabi.tar.bz2 \
    && rm /gcc-arm-none-eabi.tar.bz2 \
    && mv /gcc-arm-none-eabi-* /gcc-arm-none-eabi \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt-get update \
    && apt-get install --yes \
        g++-9 \
        mingw-w64 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 \
    && update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix \
    && update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix \
    && update-alternatives --set i686-w64-mingw32-gcc /usr/bin/i686-w64-mingw32-gcc-posix \
    && update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix \
# Additional tools
    && gem install roo \
# Robot Framework
    # Jython
    && wget "https://repo1.maven.org/maven2/org/python/jython-installer/${JYTHON_VERSION}/jython-installer-${JYTHON_VERSION}.jar" \
    && java -jar "jython-installer-${JYTHON_VERSION}.jar" -s -d /opt/jython \
    # Robot
    && python3 -m pip install robotframework-python3 \
    # Robot libraries
    && python3 -m pip install \
        robotframework-requests \
        robotframework-websocketclient \
        robotframework-rammbock-py3 \
# VNC
    && mkdir ~/.vnc \
    && echo "12345" | vncpasswd -f >> ~/.vnc/passwd \
    && chmod 600 ~/.vnc/passwd \
    && printf '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\nstartxfce4 &' > ~/.vnc/xstartup \
    && chmod +x ~/.vnc/xstartup \
# noVNC
    && cd /opt \
    && git clone https://github.com/kanaka/noVNC \
    && cd /opt/noVNC/utils \
    && git clone https://github.com/novnc/websockify \
    && cp /opt/noVNC/vnc_lite.html /opt/noVNC/index.html \
    && sed -i -e 's/prompt(\"Password Required:\")/\"12345\"/g' /opt/noVNC/index.html \
## FFmpeg
    && apt-get -y install \
        autoconf \
        automake \
        build-essential \
        cmake \
        git-core \
        libass-dev \
        libfreetype6-dev \
        libgnutls28-dev \
        libsdl2-dev \
        libtool \
        libva-dev \
        libvdpau-dev \
        libvorbis-dev \
        libxcb1-dev \
        libxcb-shm0-dev \
        libxcb-xfixes0-dev \
        pkg-config \
        texinfo \
        wget \
        yasm \
        zlib1g-dev \
        nasm \
        libx264-dev \
    && cd /tmp \
    && git clone https://github.com/FFmpeg/FFmpeg.git \
    && cd FFmpeg \
    && ./configure \
        --enable-libxcb \
        --enable-gpl \
        --enable-libx264 \
        --disable-x86asm \
        --enable-nonfree \
    && make \
    && make install \
    && cd /tmp \
    && rm -rf ./FFmpeg \
# Visual Studio Code
    && mkdir -p /vscode/data /vscode/extensions \
    && chmod -R 0777 /vscode \
    && curl -fsSL https://code-server.dev/install.sh | sh \
    && chmod 0777 /start-vscode.sh \
# Permissions
    && chmod +x /*.sh \
# libpng
    && dpkg -i /tmp/libpng12-0.deb \
    && rm /tmp/libpng12-0.deb \
# Various configurations
    && touch /root/.Xauthority \
    && update-alternatives --set x-terminal-emulator /usr/bin/xfce4-terminal.wrapper \
# Info files
    && mkdir -p /info \
    && touch /info/IsDocker \
# Cleanup
    && apt-get purge -y pm-utils xscreensaver* \
    && rm -rf /var/lib/apt/lists/*

CMD [ "/run.sh" ]